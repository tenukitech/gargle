/************************************************************************
 * Copyright (c) 2012 Tenuki Technologies, LLC. All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *    * Neither the name of Tenuki Technologies, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ************************************************************************/

package output

import (
	"fmt"
	"os"
)

// Exported methods
func Debug(a ...interface{}) (n int, err error) {
	if DebugLevel > 0 {
		return lprint("DEBUG", a...)
	}
	return
}

func Debugf(format string, a ...interface{}) (n int, err error) {
	if DebugLevel > 0 {
		return lprintf("DEBUG", format, a...)
	}
	return
}

func Debug2(a ...interface{}) (n int, err error) {
	if DebugLevel > 1 {
		return lprint("DEBUG", a...)
	}
	return
}

func Debug2f(format string, a ...interface{}) (n int, err error) {
	if DebugLevel > 1 {
		return lprintf("DEBUG", format, a...)
	}
	return
}

func Debug3(a ...interface{}) (n int, err error) {
	if DebugLevel > 2 {
		return lprint("DEBUG", a...)
	}
	return
}

func Debug3f(format string, a ...interface{}) (n int, err error) {
	if DebugLevel <= 0 {
		return lprintf("DEBUG", format, a...)
	}
	return
}

func Fatal(a ...interface{}) (n int, err error) {
	n, err = lprint("FATAL", a...)
	os.Exit(1)
	return
}

func SetDebugLevel(n int) {
	DebugLevel = n
	Debugf("Debug level is %d", n)
}

func Warn(a ...interface{}) (n int, err error) {
	n, err = lprint("WARN ", a...)
	return
}

func Warnf(a ...interface{}) (n int, format string, err error) {
	n, err = lprintf("WARN", format, a...)
	return
}

func Error(a ...interface{}) (n int, err error) {
	n, err = lprint("ERROR", a...)
	return
}

func Errorf(a ...interface{}) (n int, format string, err error) {
	n, err = lprintf("ERROR", format, a...)
	return
}

// Package variables
var DebugLevel = 0

// Non-exported methods
func lprint(level string, a ...interface{}) (n int, err error) {
	fmt.Print("[" + level + "] ")
	return fmt.Println(a...)
}

func lprintf(level string, format string, a ...interface{}) (n int, err error) {
	fmt.Print("[" + level + "] ")
	n, err = fmt.Printf(format, a...)
	if format[len(format)-1] != '\n' {
		fmt.Println()
	}
	return
}
