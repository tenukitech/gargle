/************************************************************************
 * Copyright (c) 2012 Tenuki Technologies, LLC. All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *    * Neither the name of Tenuki Technologies, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ************************************************************************/

package repl

import (
	"bitbucket.org/tenukitech/gargle/interpreter"
	"bitbucket.org/tenukitech/gargle/output"
	"errors"
	"fmt"
	"github.com/mitchellh/go-homedir"
	"github.com/peterh/liner"
	"io"
	"os"
)

func Run() error {
	line := liner.NewLiner()
	defer line.Close()

	// line.SetCompleter(func(line string) (c []string) {
	//   for _, n := range names {
	//     if strings.HasPrefix(n, strings.ToLower(line)) {
	//       c = append(c, n)
	//     }
	//   }
	//   return
	//   })

	history_filename, _ := homedir.Expand("~/.gargle_history")
	if f, err := os.Open(history_filename); err == nil {
		line.ReadHistory(f)
		f.Close()
	}

	line.SetCtrlCAborts(true)

	line_number := 0
	ctrl_c_primed := false

	interp := interpreter.NewInterpreter()

	for {
		cmd, err := line.Prompt(fmt.Sprintf("(%d) >>> ", line_number))

		switch err {
		case liner.ErrPromptAborted:
			if ctrl_c_primed {
				return errors.New("Aborted with CTRL-C!")
			} else {
				ctrl_c_primed = true
				continue
			}
		case io.EOF:
			fmt.Println("buh-bye!")
			return nil
		case nil:
			switch cmd {
			case "quit":
				fmt.Println("buh-bye!")
				return nil
			default:
				output.Debugf("Got input '%s'\n", cmd)
				result, complete, err := interp.Eval(cmd)

				if complete {
					fmt.Println("Complete!")
				}

				if err != nil {
					fmt.Println("ERROR:", err)
				} else {
					if result != nil {
						fmt.Printf("%#v (%T)\n", result, result)
					} else {
						fmt.Printf("{}")
					}
				}
			}
		}
		ctrl_c_primed = false
		line_number += 1

		if f, err := os.Create(history_filename); err != nil {
			output.Warn("Error writing history file: ", err)
		} else {
			line.WriteHistory(f)
			f.Close()
		}
	}

	return nil
}
