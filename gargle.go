/*******************************************************************************
Copyright (c) 2012 Tenuki Technologies, LLC. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

   * Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above
copyright notice, this list of conditions and the following disclaimer
in the documentation and/or other materials provided with the
distribution.
   * Neither the name of Tenuki Technologies, LLC nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/

package gargle

import (
	"bitbucket.org/tenukitech/gargle/interpreter"
	"bitbucket.org/tenukitech/gargle/output"
	"bitbucket.org/tenukitech/gargle/repl"
	"flag"
	"fmt"
	"strings"
)

var debugLevel = flag.Int("debug", 1, "Set debug output level")
var eval = flag.Bool("eval", true, "Statement to eval")
var verbose = flag.Bool("verbose", true, "Print verbose output")

func Main() int {
	flag.Parse()

	output.SetDebugLevel(*debugLevel)

	args := flag.Args()
	if len(args) == 0 || args[0] == "-" {
		err := repl.Run()
		if err == nil {
			return 0
		} else {
			fmt.Println(err)
			return 1
		}
	} else if *eval {
		interp := interpreter.NewInterpreter()
		result, _, err := interp.Eval(strings.Join(args, " "))
		if err == nil {
			fmt.Printf("RESULT: %v (%T)\n", result, result)
		} else {
			fmt.Println(err)
		}
	} else if len(args) == 1 {
		output.Debugf("Would try to interpret file %s", args[0])
	} else {
		output.Fatal("Multiple source files specified, but result for more than one file is not defined.")
	}

	return 0
}
