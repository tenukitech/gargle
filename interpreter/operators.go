/************************************************************************
 * Copyright (c) 2012 Tenuki Technologies, LLC. All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *    * Neither the name of Tenuki Technologies, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ************************************************************************/

package interpreter

import (
	"fmt"
	"reflect"
)

/* Notes:

+    sum                    integers, floats, complex values, strings
-    difference             integers, floats, complex values
*    product                integers, floats, complex values
/    quotient               integers, floats, complex values
%    remainder              integers

&    bitwise AND            integers
|    bitwise OR             integers
^    bitwise XOR            integers
&^   bit clear (AND NOT)    integers

<<   left shift             integer << unsigned integer
>>   right shift            integer >> unsigned integer
*/

/********************************************************************
 * Implement the "+" operator
 *
 * +    sum                    integers, floats, complex values, strings
 ********************************************************************/
func addOperator(x, y interface{}) (result interface{}, err error) {
	if reflect.TypeOf(x) == reflect.TypeOf(y) {
		switch x.(type) {
		case int64, int32:
			result = x.(int64) + y.(int64)
		case float64:
			result = x.(float64) + y.(float64)
		case string:
			result = x.(string) + y.(string)
		default:
			err = fmt.Errorf("Type %T not supported for + operator at this time", y)
		}
	} else {
		err = fmt.Errorf("Cannot add mixed types %T and %T.", x, y)
	}
	return
}

/********************************************************************
 * &    bitwise AND            integers
 ********************************************************************/
func andOperator(x, y interface{}) (result interface{}, err error) {
	if (reflect.TypeOf(x).Kind() == reflect.Int64) && (reflect.TypeOf(y).Kind() == reflect.Int64) {
		result = x.(int64) & y.(int64)
	} else {
		err = fmt.Errorf("invalid operand for & operator")
	}
	return
}

/********************************************************************
 * Implement the "*" operator
 ********************************************************************/
func mulOperator(x, y interface{}) (result interface{}, err error) {
	if reflect.TypeOf(x) == reflect.TypeOf(y) {
		switch x.(type) {
		case int64, int32:
			result = x.(int64) * y.(int64)
		case float64:
			result = x.(float64) * y.(float64)
		default:
			err = fmt.Errorf("Type %T not supported for * operator at this time", y)
		}
	} else {
		err = fmt.Errorf("Cannot multiply mixed types %T and %T.", x, y)
	}
	return
}

/********************************************************************
 * |    bitwise OR             integers
 ********************************************************************/
func orOperator(x, y interface{}) (result interface{}, err error) {
	if (reflect.TypeOf(x).Kind() == reflect.Int64) && (reflect.TypeOf(y).Kind() == reflect.Int64) {
		result = x.(int64) | y.(int64)
	} else {
		err = fmt.Errorf("invalid operand for & operator")
	}
	return
}

/********************************************************************
 * Implement the "/" operator
 ********************************************************************/
func quoOperator(x, y interface{}) (result interface{}, err error) {
	if reflect.TypeOf(x) == reflect.TypeOf(y) {
		switch x.(type) {
		case int64, int32:
			result = x.(int64) / y.(int64)
		case float64:
			result = x.(float64) / y.(float64)
		default:
			err = fmt.Errorf("Type %T not supported for / operator at this time", y)
		}
	} else {
		err = fmt.Errorf("Cannot divide mixed types %T and %T.", x, y)
	}
	return
}

/********************************************************************
 * Implement the "%" operator
 ********************************************************************/
func remOperator(x, y interface{}) (result interface{}, err error) {
	if reflect.TypeOf(x) == reflect.TypeOf(y) {
		switch x.(type) {
		case int64, int32:
			result = x.(int64) % y.(int64)
		default:
			err = fmt.Errorf("Type %T not supported for % operator at this time", y)
		}
	} else {
		err = fmt.Errorf("Cannot get remainder for mixed types %T and %T.", x, y)
	}
	return
}

/********************************************************************
 * Implement the "-" operator
 ********************************************************************/
func subOperator(x, y interface{}) (result interface{}, err error) {
	if reflect.TypeOf(x) == reflect.TypeOf(y) {
		switch x.(type) {
		case int64, int32:
			result = x.(int64) - y.(int64)
		case float64:
			result = x.(float64) - y.(float64)
		default:
			err = fmt.Errorf("Type %T not supported for - operator at this time", y)
		}
	} else {
		err = fmt.Errorf("Cannot subtract mixed types %T and %T.", x, y)
	}
	return
}

/********************************************************************
 * ^    bitwise XOR            integers
 ********************************************************************/
func xorOperator(x, y interface{}) (result interface{}, err error) {
	if (reflect.TypeOf(x).Kind() == reflect.Int64) && (reflect.TypeOf(y).Kind() == reflect.Int64) {
		result = x.(int64) ^ y.(int64)
	} else {
		err = fmt.Errorf("invalid operand for ^ operator")
	}
	return
}
