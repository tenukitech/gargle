/************************************************************************
 * Copyright (c) 2012 Tenuki Technologies, LLC. All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *    * Neither the name of Tenuki Technologies, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ************************************************************************/
package interpreter

import (
	"bitbucket.org/tenukitech/gargle/output"
	"bitbucket.org/tenukitech/gargle/parser"
	"go/ast"
	"regexp"
	// "fmt"
	// "go/token"
)

var packageStatementRE = regexp.MustCompile(`^package\s+(.*)$(.*)`)

type Interpreter struct {
	Packages *PackageMap
	Current  *Package
}

func NewInterpreter() (interp *Interpreter) {
	interp = new(Interpreter)
	interp.Packages = NewPackageMap()
	mainPkg := NewPackage("main", NewScope(nil), map[string]*ast.Object{}, map[string]*ast.File{})
	interp.Packages.Insert(mainPkg)
	interp.Current = mainPkg
	return interp
}

func (interp *Interpreter) Eval(statement string) (result interface{}, complete bool, err error) {
	if packageStatementRE.Match([]byte(statement)) {
		// easiest way to do this since the parser package doesn't seem to support
		// package statements without the whole file in place.
		//
		// Once this is more fleshed out, it will mean that packages in Gargle
		// are implicitly open -- subject to change after the fact.  I think this is
		// a good thing.
		packageName := string(packageStatementRE.FindSubmatch([]byte(statement))[1])
		output.Debugf("Ignoring package decl for package %s", packageName)
		statement = string(packageStatementRE.ReplaceAll([]byte(statement), []byte("$2")))
		return
	}

	ast, err := parser.ParseStatement(statement)
	if err == nil {
		result, err = interp.evaluate(ast)
		complete = true
		return result, complete, err
	} else {
		complete = true
		output.Error("Caught error: ", err)
	}
	return
}
