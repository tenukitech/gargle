/************************************************************************
 * Copyright (c) 2012 Tenuki Technologies, LLC. All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *    * Neither the name of Tenuki Technologies, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ************************************************************************/

package interpreter

import (
	"bitbucket.org/tenukitech/gargle/output"
	"fmt"
	"go/ast"
	"go/token"
	//"reflect"
	"strconv"
)

func (interp *Interpreter) evaluate(statement interface{}) (result interface{}, err error) {
	output.Debug2("Entering evalute")
	switch statement.(type) {
	case *ast.AssignStmt:
		result, err = interp.evaluateAssignStmt(statement.(*ast.AssignStmt))
	case *ast.BasicLit:
		result, err = interp.evaluateBasicLit(statement.(*ast.BasicLit))
	case *ast.BinaryExpr:
		result, err = interp.evaluateBinaryExpr(statement.(*ast.BinaryExpr))
	case *ast.ExprStmt:
		result, err = interp.evaluateExprStmt(statement.(*ast.ExprStmt))
	case *ast.Ident:
		result, err = interp.evaluateIdent(statement.(*ast.Ident))
	case *ast.ParenExpr:
		result, err = interp.evaluateParenExpr(statement.(*ast.ParenExpr))
	case []ast.Expr:
		length := len(statement.([]ast.Expr))
		retval := make([]interface{}, length)
		for i, statement := range statement.([]ast.Expr) {
			retval[i], err = interp.evaluate(statement)
			if err != nil {
				return nil, err
			}
		}
		result = retval
	case []ast.Stmt:
		for _, statement := range statement.([]ast.Stmt) {
			output.Debug3f("statement: %v", statement)
			result, err = interp.evaluate(statement)
			if err == nil {
				output.Debug3f("In evaluate parsing []ast.Stmt: %s", err)
				break
			}
		}
	default:
		err = fmt.Errorf("evaluate: Expression type '%T' not yet supported: %v", statement, statement)
	}

	return
}

func (interp *Interpreter) evaluateAssignStmt(statement *ast.AssignStmt) (result interface{}, err error) {
	rhs, err := interp.evaluate(statement.Rhs)
	if err != nil {
		return nil, err
	}

	switch statement.Tok {
	case token.DEFINE:
		lhsList := statement.Lhs
		rhsList := rhs.([]interface{})
		var resultList = make([]interface{}, len(lhsList))

		for idx, ident := range lhsList {
			obj := ast.NewObj(ast.Var, ident.(*ast.Ident).Name)
			obj.Data = rhsList[idx]
			obj.Decl = statement
			obj.Type = nil
			output.Debugf("OBJ: %#v", obj)
			interp.Current.Scope.Insert(obj)
			resultList[idx] = rhsList[idx]
		}
		if len(resultList) == 1 {
			result = resultList[0]
		} else {
			result = resultList
		}
	case token.ASSIGN:
		output.Debug("Token assign caught")
	default:
		output.Debug("Unknown token caught in evaluateAssignStmt")
	}
	return
}

func (interp *Interpreter) evaluateBasicLit(statement *ast.BasicLit) (result interface{}, err error) {
	output.Debug2("Entering evaluateBasicLit")
	switch statement.Kind {
	case token.CHAR:
		result = statement.Value[0]
	case token.FLOAT:
		result, err = strconv.ParseFloat(statement.Value, 64)
	case token.IMAG:
		err = fmt.Errorf("Imaginary literals not supported yet.")
	case token.INT:
		result, err = strconv.ParseInt(statement.Value, 0, 64)
	case token.STRING:
		result = statement.Value[1 : len(statement.Value)-1]
	default:
		err = fmt.Errorf("evaluateBasicLit: Unsupported Token type %d", statement.Kind)
	}
	return
}

func (interp *Interpreter) evaluateBinaryExpr(statement *ast.BinaryExpr) (result interface{}, err error) {
	output.Debug2("Entering evaluateBinaryExpr")
	x, err := interp.evaluate(statement.X)
	if err != nil {
		return
	}
	y, err := interp.evaluate(statement.Y)
	if err != nil {
		return
	}

	switch statement.Op {
	case token.ADD:
		result, err = addOperator(x, y)
	case token.AND:
		result, err = andOperator(x, y)
	case token.SUB:
		result, err = subOperator(x, y)
	case token.MUL:
		result, err = mulOperator(x, y)
	case token.QUO:
		result, err = quoOperator(x, y)
	case token.REM:
		result, err = remOperator(x, y)
	default:
		err = fmt.Errorf("evaluateBinaryExpression: Unsupported operator %d in BinaryExpression", int(statement.Op))
	}
	return
}

func (interp *Interpreter) evaluateExprStmt(node *ast.ExprStmt) (result interface{}, err error) {
	output.Debug3("Entering evaluateExprStmt")
	return interp.evaluate(node.X)
}

func (interp *Interpreter) evaluateIdent(node *ast.Ident) (result interface{}, err error) {
	output.Debugf("EvaluateIdent %#v", node)
	result = interp.Current.Scope.Lookup(node.Name).Data
	return
}

func (interp *Interpreter) evaluateParenExpr(statement *ast.ParenExpr) (result interface{}, err error) {
	output.Debug2("Entering evaluateParenExpr")

	return interp.evaluate(statement.X)
}
