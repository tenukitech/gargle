/************************************************************************
 * Copyright (c) 2012 Tenuki Technologies, LLC. All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *    * Neither the name of Tenuki Technologies, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ************************************************************************/

package interpreter

import (
	"go/ast"
)

// Scope implements a scope for a declaration as a wrapper around ast.Scope
type Scope struct {
	*ast.Scope
}

func NewScope(outer *Scope) *Scope {
	if outer != nil {
		astScope := ast.NewScope(outer.Scope)
		return &Scope{astScope}
	} else {
		astScope := ast.Scope{
			Outer:   nil,
			Objects: make(map[string]*ast.Object, 4),
		}
		return &Scope{
			&astScope,
		}
	}
}

// Package implements a wrapper around the ast packages Package struct.
type Package struct {
	*ast.Package
}

func NewPackage(name string, scope *Scope, imports map[string]*ast.Object, files map[string]*ast.File) *Package {
	astPkg := ast.Package{
		Name:    name,
		Scope:   scope.Scope,
		Imports: imports,
		Files:   files,
	}

	pkg := Package{&astPkg}
	return &pkg
}

// PackageMap is Gargles structure to maintain a map of packages.
type PackageMap map[string]*Package

func NewPackageMap() *PackageMap {
	m := make(PackageMap)
	return &m
}

func (pkgMap PackageMap) Insert(pkg *Package) {
	pkgMap[pkg.Name] = pkg
}
