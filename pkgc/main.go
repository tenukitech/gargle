/************************************************************************
 * Copyright (c) 2012 Tenuki Technologies, LLC. All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *    * Neither the name of Tenuki Technologies, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ************************************************************************/

package main

import (
	"flag"
	"fmt"
	"go/ast"
	// "go/doc"
	"go/parser"
	// "io/ioutil"
	"go/token"
	// "log"
	"os"
	"path/filepath"
)

var outputPath = flag.String("output", "builtins", "Output file for stubs")
var srcPath = flag.String("src", os.ExpandEnv("${GOROOT}/src/pkg"), "Source path to check")
var verbose = flag.Bool("verbose", true, "Print verbose output")

func main() {
	flag.Parse()

	// outputFilePath, err := filepath.Abs(*outputFile)
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// outFile, err := os.OpenFile(outputFilePath, os.O_CREATE, 0666)
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// outFile.Close()

	filepath.Walk(*srcPath, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			fmt.Println("Processing", path)
			fset := token.NewFileSet()
			packageMap, err := parser.ParseDir(fset, path, nil, parser.AllErrors)
			if err != nil {
				fmt.Println(err)
				return err
			}

			for pkgname, pkg := range packageMap {
				if err == nil {
					fmt.Printf("import \"%s\"\n", pkgname)
				} else {
					fmt.Println(err)
				}

				file := ast.MergePackageFiles(pkg, ast.FilterFuncDuplicates)
				for _, decl := range file.Decls {
					switch decl.(type) {
					case *ast.FuncDecl:
						fmt.Println("func", decl.(ast.FuncDecl).Name)
					}

				}

			}
		}
		return nil
	})
	return

}
